﻿using Microsoft.Extensions.Logging;
using WEB_T_MT_API_Integrator.Model;

namespace WEB_T_MT_API_Integrator.Service
{
    /// <summary>
    /// Abstract translation provider engine.
    /// </summary>
    public abstract class AbstractTranslationService
    {
        /// <summary>
        /// Char limit which determines when to split translatable strings into separate requests.
        /// </summary>
        protected readonly int MaxCharsPerRequest = 60000;

        /// <summary>
        /// HTTP client request timeout in seconds.
        /// </summary>
        protected readonly int RequestTimeout = 600;

        protected readonly ILogger<AbstractTranslationService> Logger;

        protected AbstractTranslationService(ILogger<AbstractTranslationService> logger)
        {
            Logger = logger;
        }

        /// <summary>
        /// Translates string array from source to target language.
        /// </summary>
        /// <param name="from">Langcode of source language.</param>
        /// <param name="to">Langcode of target language.</param>
        /// <param name="values">Array of strings to be translated.</param>
        /// <returns>Array of translated strings.</returns>
        protected abstract Task<List<string>> SendTranslationRequestAsync(string from, string to, List<string> values);

        /// <summary>
        /// Retrieves supported language directions.
        /// </summary>
        /// <returns>Supported language directions.</returns>
        protected abstract Task<object> SendLanguageDirectionRequestAsync();

        /// <summary>
        /// Translates string array from source to target language using one or more HTTP requests.
        /// </summary>
        /// <param name="from">Langcode of source language.</param>
        /// <param name="to">Langcode of target language.</param>
        /// <param name="values">Array of strings to be translated.</param>
        /// <returns>Array of translated strings.</returns>
        public async Task<List<string>> TranslateAsync(string from, string to, List<string> values)
        {
            int currentCharLen = 0;
            var requestValueLists = new List<List<string>>();
            var currentList = new List<string>();

            // Split into multiple arrays not exceeding max char limit.
            foreach (var value in values)
            {
                int chars = value.Length;
                if (currentCharLen + chars > MaxCharsPerRequest)
                {
                    requestValueLists.Add(currentList);
                    currentList = new List<string> { value };
                    currentCharLen = chars;
                }
                else
                {
                    currentList.Add(value);
                    currentCharLen += chars;
                }
            }
            if (currentList.Any())
            {
                requestValueLists.Add(currentList);
            }

            // Send requests and merge results together.
            var fullResult = new List<string>();
            int requestCount = requestValueLists.Count;

            for (int i = 0; i < requestCount; i++)
            {
                var containsOnlyEmptyStrings = !requestValueLists[i].Any(s => !string.IsNullOrEmpty(s));

                if (containsOnlyEmptyStrings)
                {
                    fullResult.AddRange(requestValueLists[i]);
                }
                else
                {
                    Logger.LogDebug($"Sending {from}->{to} translation request {i + 1}/{requestCount} ({requestValueLists[i].Count} strings)");

                    var translations = await SendTranslationRequestAsync(from, to, requestValueLists[i]);

                    if (translations == null || !translations.Any())
                    {
                        int retriesLeft = 2;
                        while (retriesLeft > 0)
                        {
                            Logger.LogWarning($"Translation request failed, retrying... (retries left: {retriesLeft})");
                            translations = await SendTranslationRequestAsync(from, to, requestValueLists[i]);
                            retriesLeft--;
                        }

                        // Do not continue if one of requests fail.
                        if (translations == null || !translations.Any())
                        {
                            if (requestCount > 1)
                            {
                                Logger.LogError("One of the translation requests failed.");
                            }
                            return new List<string>();
                        }
                    }
                    // Merge with previous translations.
                    fullResult.AddRange(translations);
                }
            }
            return fullResult;
        }

        /// <summary>
        /// Map language direction response to Generic API response.
        /// </summary>
        /// <param name="response">Language direction response from MT provider.</param>
        /// <returns>Mapped response.</returns>
        protected virtual LanguageDirectionsResponse MapLanguageDirectionResponse(object response)
        {
            return (LanguageDirectionsResponse) response;
        }

        /// <summary>
        /// Retrieves supported MT system list.
        /// </summary>
        /// <returns>Supported MT system list.</returns>
        public async Task<LanguageDirectionsResponse> GetSupportedEngineListAsync()
        {
            var languageDirections = await SendLanguageDirectionRequestAsync();
            return MapLanguageDirectionResponse(languageDirections);
        }
    }
}
