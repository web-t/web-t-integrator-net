﻿using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using Microsoft.Extensions.Logging;
using WEB_T_MT_API_Integrator.Model;

namespace WEB_T_MT_API_Integrator.Service
{
    public class CustomProviderService : AbstractTranslationService
    {
        private readonly string _url;
        private readonly string _apiKey;

        public CustomProviderService(string url, string apiKey, ILogger<AbstractTranslationService> logger) : base(logger)
        {
            _url = url;
            _apiKey = apiKey;
        }
        /// <inheritdoc />
        protected override async Task<List<string>> SendTranslationRequestAsync(string from, string to, List<string> values)
        {
            var data = new
            {
                srcLang = from.Split('_')[0],
                trgLang = to.Split('_')[0],
                text = values
            };

            if (string.IsNullOrEmpty(_url) || string.IsNullOrEmpty(_apiKey))
            {
                throw new Exception("Translation provider not configured!");
            }

            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add("X-API-Key", _apiKey);
                var requestBody = new StringContent(JsonSerializer.Serialize(data), Encoding.UTF8, "application/json");

                using (var response = await httpClient.PostAsync($"{_url}/translate/text", requestBody))
                {
                    if (!response.IsSuccessStatusCode)
                    {
                        if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                        {
                            Logger.LogError("Translation endpoint not found!");
                            return new List<string>();
                        }

                        Logger.LogError($"Invalid response status code from translation provider: {response.StatusCode}");
                        return new List<string>();
                    }

                    var jsonResponse = await response.Content.ReadAsStringAsync();
                    var translations = JsonSerializer.Deserialize<TranslationResponse>(jsonResponse);
                    var translationList = new List<string>();

                    foreach (var translation in translations.Translations)
                    {
                        translationList.Add(translation.Translation);
                    }

                    return translationList;
                }
            }
        }

        /// <inheritdoc />
        protected override async Task<object> SendLanguageDirectionRequestAsync()
        {
            if (string.IsNullOrEmpty(_url) || string.IsNullOrEmpty(_apiKey))
            {
                throw new Exception("Translation provider not configured!");
            }

            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add("X-API-Key", _apiKey);

                using (var response = await httpClient.GetAsync($"{_url}/translate/language-directions"))
                {
                    if (!response.IsSuccessStatusCode)
                    {
                        Logger.LogError($"Error retrieving language directions: {response.StatusCode}");
                        return null;
                    }

                    var jsonResponse = await response.Content.ReadAsStringAsync();
                    return JsonSerializer.Deserialize<LanguageDirectionsResponse>(jsonResponse);
                }
            }
        }
    }
}