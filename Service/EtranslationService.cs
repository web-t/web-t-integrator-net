﻿using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using Microsoft.Extensions.Logging;
using WEB_T_MT_API_Integrator.Model;

namespace WEB_T_MT_API_Integrator.Service
{
    /// <summary>
    /// Etranslation MT engine integration.
    /// </summary>
    public class EtranslationService : AbstractTranslationService
    {
        /// <summary>
        /// Error response code dictionary.
        /// </summary>
        private static readonly Dictionary<int, string> ErrorMap = new()
        {
            { -20000, "Source language not specified" },
            { -20001, "Invalid source language" },
            { -20002, "Target language(s) not specified" },
            { -20003, "Invalid target language(s)" },
            { -20004, "DEPRECATED" },
            { -20005, "Caller information not specified" },
            { -20006, "Missing application name" },
            { -20007, "Application not authorized to access the service" },
            { -20008, "Bad format for ftp address" },
            { -20009, "Bad format for sftp address" },
            { -20010, "Bad format for http address" },
            { -20011, "Bad format for email address" },
            { -20012, "Translation request must be text type, document path type or document base64 type and not several at a time" },
            { -20013, "Language pair not supported by the domain" },
            { -20014, "Username parameter not specified" },
            { -20015, "Extension invalid compared to the MIME type" },
            { -20016, "DEPRECATED" },
            { -20017, "Username parameter too long" },
            { -20018, "Invalid output format" },
            { -20019, "Institution parameter too long" },
            { -20020, "Department number too long" },
            { -20021, "Text to translate too long" },
            { -20022, "Too many FTP destinations" },
            { -20023, "Too many SFTP destinations" },
            { -20024, "Too many HTTP destinations" },
            { -20025, "Missing destination" },
            { -20026, "Bad requester callback protocol" },
            { -20027, "Bad error callback protocol" },
            { -20028, "Concurrency quota exceeded" },
            { -20029, "Document format not supported" },
            { -20030, "Text to translate is empty" },
            { -20031, "Missing text or document to translate" },
            { -20032, "Email address too long" },
            { -20033, "Cannot read stream" },
            { -20034, "Output format not supported" },
            { -20035, "Email destination tag is missing or empty" },
            { -20036, "HTTP destination tag is missing or empty" },
            { -20037, "FTP destination tag is missing or empty" },
            { -20038, "SFTP destination tag is missing or empty" },
            { -20039, "Document to translate tag is missing or empty" },
            { -20040, "Format tag is missing or empty" },
            { -20041, "The content is missing or empty" },
            { -20042, "Source language defined in TMX file differs from request" },
            { -20043, "Source language defined in XLIFF file differs from request" },
            { -20044, "Output format is not available when quality estimate is requested. It should be blank or 'xslx'" },
            { -20045, "Quality estimate is not available for text snippet" },
            { -20046, "Document too big (>20Mb)" },
            { -20047, "Quality estimation not available" },
            { -40010, "Too many segments to translate" },
            { -80004, "Cannot store notification file at specified FTP address" },
            { -80005, "Cannot store notification file at specified SFTP address" },
            { -80006, "Cannot store translated file at specified FTP address" },
            { -80007, "Cannot store translated file at specified SFTP address" },
            { -90000, "Cannot connect to FTP" },
            { -90001, "Cannot retrieve file at specified FTP address" },
            { -90002, "File not found at specified address on FTP" },
            { -90007, "Malformed FTP address" },
            { -90012, "Cannot retrieve file content on SFTP" },
            { -90013, "Cannot connect to SFTP" },
            { -90014, "Cannot store file at specified FTP address" },
            { -90015, "Cannot retrieve file content on SFTP" },
            { -90016, "Cannot retrieve file at specified SFTP address" }
        };

        /// <summary>
        /// Service API URL.
        /// </summary>
        private static readonly string ApiUrl = "https://webgate.ec.europa.eu/etranslation/si";

        /// <summary>
        /// HTML tag part between translations.
        /// </summary>
        private static readonly string Delimiter = "<param name=\"webt-delimiter\" />";

        /// <summary>
        /// HTML tag which precedes every string translation.
        /// </summary>
        private static readonly string Prefix = "<html>";

        /// <summary>
        /// HTML tag which closes every string translation.
        /// </summary>
        private static readonly string Suffix = "</html>";

        /// <summary>
        /// Application name for eTranslation API.
        /// </summary>
        private readonly string _applicationName;

        /// <summary>
        /// Password for eTranslation API.
        /// </summary>
        private readonly string _password;

        /// <summary>
        /// HTTP endpoint URL which will receive eTranslation response.
        /// </summary>
        private readonly string _endpointUrl;

        /// <summary>
        /// Callback function which waits for async eTranslation response.
        /// </summary>
        private readonly Func<string, string, Task<string>> _awaitResponseCallback;

        /// <summary>
        /// Constructor for EtranslationService.
        /// </summary>
        /// <param name="applicationName">Application name for eTranslation API.</param>
        /// <param name="password">Password for eTranslation API.</param>
        /// <param name="endpointUrl">HTTP endpoint URL which will receive eTranslation response.</param>
        /// <param name="awaitResponseCallback">Callback function which waits for async eTranslation response.</param>
        public EtranslationService(string applicationName, string password, string endpointUrl, Func<string, string, Task<string>> awaitResponseCallback, ILogger<AbstractTranslationService> logger)
            : base(logger)
        {
            _applicationName = applicationName;
            _password = password;
            _endpointUrl = endpointUrl;
            _awaitResponseCallback = awaitResponseCallback;
        }

        /// <summary>
        /// Translates string array from source to target language using eTranslation.
        /// </summary>
        /// <param name="from">Langcode of source language.</param>
        /// <param name="to">Langcode of target language.</param>
        /// <param name="values">Array of strings to be translated.</param>
        /// <returns>Array of translated strings.</returns>
        /// <exception cref="Exception">If credentials are not configured.</exception>
        protected override async Task<List<string>> SendTranslationRequestAsync(string from, string to, List<string> values)
        {
            if (string.IsNullOrEmpty(_applicationName) || string.IsNullOrEmpty(_password))
            {
                throw new Exception("eTranslation credentials not configured!");
            }

            string langFrom = from.Split('_')[0];
            string langTo = to.Split('_')[0];

            string id = Guid.NewGuid().ToString();
            string content = EncodeRequestBody(values);

            if (string.IsNullOrEmpty(content))
            {
                return new List<string>();
            }

            string postBody = GetPostBody(id, langFrom, langTo, content);
            using var client = new HttpClient(new DigestAuthMessageHandler(new HttpClientHandler(), _applicationName, _password));

            var responseMessage = await client.PostAsync($"{ApiUrl}/translate", new StringContent(postBody, Encoding.UTF8, "application/json"));
            string response = await responseMessage.Content.ReadAsStringAsync();

            var requestId = 0;
            if (!responseMessage.IsSuccessStatusCode || !int.TryParse(response, out requestId) || requestId < 0)
            {
                string message = ErrorMap.TryGetValue(requestId, out string errorMsg) ? errorMsg : response;
                Logger.LogError($"Invalid request response from eTranslation: {response} [status: {(int)responseMessage.StatusCode}, message: {message}, error: {responseMessage.ReasonPhrase}]");
                return new List<string>();
            }

            Logger.LogDebug($"eTranslation request successful ({requestId}) [ID={id}]");

            string callbackResponse = await _awaitResponseCallback(id, to);
            return callbackResponse != null ? DecodeResponse(callbackResponse) : new List<string>();
        }
        /// <inheritdoc />
        protected override async Task<object> SendLanguageDirectionRequestAsync()
        {
            using var client = new HttpClient(new DigestAuthMessageHandler(new HttpClientHandler(), _applicationName, _password));
            var responseMessage = await client.GetAsync($"{ApiUrl}/get-domains");
            string response = await responseMessage.Content.ReadAsStringAsync();

            if (!responseMessage.IsSuccessStatusCode)
            {
                Logger.LogError($"Error retrieving domains from eTranslation: {response} [status: {(int)responseMessage.StatusCode}]");
            }
            return JsonSerializer.Deserialize<Dictionary<string, EtranslationDomain>>(response);
        }

        /// <summary>
        /// Constructs the body of the POST request for eTranslation service.
        /// </summary>
        /// <param name="id">The unique identifier for the request.</param>
        /// <param name="langFrom">The source language code.</param>
        /// <param name="langTo">The target language code.</param>
        /// <param name="translatableString">The string to be translated.</param>
        /// <returns>The JSON serialized string of the POST request body.</returns>
        private string GetPostBody(string id, string langFrom, string langTo, string translatableString)
        {
            var document = new
            {
                content = translatableString,
                format = "html",
                filename = "translateMe"
            };

            var translationRequestBody = new
            {
                documentToTranslateBase64 = document,
                sourceLanguage = langFrom.ToUpper(),
                targetLanguages = new[] { langTo.ToUpper() },
                errorCallback = _endpointUrl,
                callerInformation = new
                {
                    application = _applicationName
                },
                destinations = new
                {
                    httpDestinations = new[] { _endpointUrl }
                },
                externalReference = id
            };
            return JsonSerializer.Serialize(translationRequestBody);
        }
        /// <inheritdoc />
        protected override LanguageDirectionsResponse MapLanguageDirectionResponse(object res)
        {
            var responseDict = res as Dictionary<string, EtranslationDomain>;
            var result = new LanguageDirectionsResponse();
            result.LanguageDirections = new List<LanguageDirection>();

            foreach (var (domain, value) in responseDict)
            {
                var langPairs = value.LanguagePairs;

                foreach (var pair in langPairs)
                {
                    var langs = pair.ToString().ToLower().Split('-');
                    result.LanguageDirections.Add(new LanguageDirection
                    {
                        SrcLang = langs[0],
                        TrgLang = langs[1],
                        Domain = value.Name
                    });
                }
            }

            return result;
        }

        /// <summary>
        /// Encodes a list of strings into a single base64-encoded HTML string.
        /// </summary>
        /// <param name="values">The list of strings to encode.</param>
        /// <returns>The base64-encoded HTML string.</returns>
        public static string EncodeRequestBody(IEnumerable<string> values)
        {
            var str = string.Join(Delimiter, values);
            var html = $"{Prefix}{str}{Suffix}";
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(html));
        }

        /// <summary>
        /// Decodes a base64-encoded HTML string into a list of strings.
        /// </summary>
        /// <param name="base64html">The base64-encoded HTML string to decode.</param>
        /// <returns>A list of decoded strings.</returns>
        private static List<string> DecodeResponse(string base64html)
        {
            var html = Encoding.UTF8.GetString(Convert.FromBase64String(base64html));
            var str = html.Substring(Prefix.Length, html.Length - Prefix.Length - Suffix.Length);
            return str.Split(new[] { Delimiter }, StringSplitOptions.None).ToList();
        }
    }

    /// <summary>
    /// Custom HTTP message handler that implements Digest Authentication.
    /// </summary>
    public class DigestAuthMessageHandler : DelegatingHandler
    {
        private readonly string _username;
        private readonly string _password;

        /// <summary>
        /// Initializes a new instance of the DigestAuthMessageHandler class.
        /// </summary>
        /// <param name="innerHandler">The inner handler to pass HTTP requests and responses.</param>
        /// <param name="username">The username for Digest Authentication.</param>
        /// <param name="password">The password for Digest Authentication.</param>
        public DigestAuthMessageHandler(HttpMessageHandler innerHandler, string username, string password)
            : base(innerHandler)
        {
            _username = username;
            _password = password;
        }

        /// <summary>
        /// Sends an HTTP request with Digest Authentication.
        /// </summary>
        /// <param name="request">The HTTP request message to send.</param>
        /// <param name="cancellationToken">The cancellation token to cancel the operation.</param>
        /// <returns>A task that represents the asynchronous operation, with a result of the HTTP response message.</returns>
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var response = await base.SendAsync(request, cancellationToken);

            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                var wwwAuthenticateHeader = response.Headers.WwwAuthenticate.FirstOrDefault();
                if (wwwAuthenticateHeader != null && wwwAuthenticateHeader.Scheme == "Digest")
                {
                    var digestHeader = DigestHeaderParser.Parse(wwwAuthenticateHeader.Parameter);
                    var digestResponse = CreateDigestResponse(request.Method.Method, request.RequestUri, digestHeader);
                    request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Digest", digestResponse);

                    response = await base.SendAsync(request, cancellationToken);
                }
            }

            return response;
        }

        /// <summary>
        /// Creates a Digest Authentication response header.
        /// </summary>
        /// <param name="method">The HTTP method (GET or POST).</param>
        /// <param name="uri">The request URI.</param>
        /// <param name="digestHeader">The parsed Digest header values.</param>
        /// <returns>The Digest Authentication response header.</returns>
        private string CreateDigestResponse(string method, Uri uri, DigestHeader digestHeader)
        {
            string nc = "00000001";
            string clientNonce = Guid.NewGuid().ToString("N");

            string ha1 = CalculateMD5Hash($"{_username}:{digestHeader.Realm}:{_password}");
            string ha2 = CalculateMD5Hash($"{method}:{uri.PathAndQuery}");

            string response = CalculateMD5Hash($"{ha1}:{digestHeader.Nonce}:{nc}:{clientNonce}:{digestHeader.Qop}:{ha2}");

            var digestHeaderValues = new Dictionary<string, string>
            {
                { "username", $"\"{_username}\"" },
                { "realm", $"\"{digestHeader.Realm}\"" },
                { "nonce", $"\"{digestHeader.Nonce}\"" },
                { "uri", $"\"{uri.PathAndQuery}\"" },
                { "algorithm", "\"MD5\"" },
                { "qop", digestHeader.Qop },
                { "nc", nc },
                { "cnonce", $"\"{clientNonce}\"" },
                { "response", $"\"{response}\"" }
            };

            return string.Join(", ", digestHeaderValues.Select(kvp => $"{kvp.Key}={kvp.Value}"));
        }

        /// <summary>
        /// Calculates the MD5 hash of the input string.
        /// </summary>
        /// <param name="input">The input string to hash.</param>
        /// <returns>The MD5 hash as a lowercase hexadecimal string.</returns>
        private string CalculateMD5Hash(string input)
        {
            using var md5 = MD5.Create();
            byte[] inputBytes = Encoding.UTF8.GetBytes(input);
            byte[] hashBytes = md5.ComputeHash(inputBytes);

            return BitConverter.ToString(hashBytes).Replace("-", "").ToLowerInvariant();
        }
    }

    /// <summary>
    /// Represents the values parsed from a Digest Authentication header.
    /// </summary>
    public class DigestHeader
    {
        public string Realm { get; set; }
        public string Nonce { get; set; }
        public string Opaque { get; set; }
        public string Qop { get; set; }
        public int NonceCount { get; set; }
        public string CNonce { get; set; }
    }

    /// <summary>
    /// Provides methods to parse Digest Authentication headers.
    /// </summary>
    public static class DigestHeaderParser
    {
        /// <summary>
        /// Parses a Digest Authentication header into a DigestHeader object.
        /// </summary>
        /// <param name="authHeader">The Digest Authentication header to parse.</param>
        /// <returns>The parsed DigestHeader object.</returns>
        public static DigestHeader Parse(string authHeader)
        {
            var parts = authHeader.Split(',');
            var digestHeader = new DigestHeader();

            foreach (var part in parts)
            {
                var index = part.IndexOf('=');
                if (index > -1)
                {
                    var key = part.Substring(0, index).Trim().ToLowerInvariant();
                    var value = part.Substring(index + 1).Trim(' ', '"');

                    switch (key)
                    {
                        case "realm":
                            digestHeader.Realm = value;
                            break;
                        case "nonce":
                            digestHeader.Nonce = value;
                            break;
                        case "opaque":
                            digestHeader.Opaque = value;
                            break;
                        case "qop":
                            digestHeader.Qop = value;
                            break;
                        case "nc":
                            digestHeader.NonceCount = int.Parse(value, System.Globalization.NumberStyles.HexNumber);
                            break;
                        case "cnonce":
                            digestHeader.CNonce = value;
                            break;
                    }
                }
            }

            return digestHeader;
        }
    }
}