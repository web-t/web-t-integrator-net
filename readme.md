# WEB-T MT API Integrator .NET library

.NET library that contains both MT provider implementations for WEB-T solutions - eTranslation and Custom provider (Generic MT API) - and test examples of their usage.

## How to use
1. Test this library by providing your MT provider data in `Program.cs` file:
1.1. Custom provider API URL & API key 
1.2. eTranslation Application name & password, HTTP response endpoint URL and response await function (see function documentation in code). 
2. Build and run solution. This will translate strings and retrieve available MT systems, then print results. 
2.1. `dotnet build "WEB-T MT API Integrator.csproj"` 
2.2. `dotnet run "WEB-T MT API Integrator.csproj"` 
3. Integrate MT API Integrator into your website translation plugin.

## License
This library is licensed under Apache 2.0 license.
