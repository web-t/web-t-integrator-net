﻿using Microsoft.Extensions.Logging;
using WEB_T_MT_API_Integrator.Service;

class Program
{
    static async Task Main(string[] args)
    {
        var loggerFactory = LoggerFactory.Create(builder =>
        {
            builder.AddConsole();
        });

        ILogger<AbstractTranslationService> logger = loggerFactory.CreateLogger<AbstractTranslationService>();

        /*
        *****************
        * DEFINE INPUTS *
        *****************
        */

        /**
        * Define translatable strings.
        */
        List<string> translatableStrings = new List<string> { "translatable string", "one two three" };
        string srcLangCode = "en_GB";
        string trgLangCode = "lv";

        /**
        *  Define Custom provider data
        */
        string customProviderUrl = "https://example.com/api";
        string customProviderApiKey = "00abc36b-7459-4e8b-ac86-b95ce1bd0c44";

        /**
        * Define eTranslation data.
        */
        string etranslationAppName = "MyEtranslationApplicationName";
        string etranslationPassword = "eTr@nslati0nPa$$worD";
        string etranslationResponseEndpointUrl = "https://example.com/etranslation-response";

        /**
        * Function that waits for eTranslation response body, retrieves and returns it.
        * Note: You should provide a separate HTTP endpoint handler function that saves eTranslation response somewhere, so that this function can find and access it.
        * For example, handler saves response into database, and this function periodically reads the database looking for response with given ID.
        *
        * This test function waits for 3s, then returns a static response.
        */
        Func<string, string, Task<string>> etranslationResponseAwaitFn = async (id, to) =>
        {
            await Task.Delay(3000);
            return EtranslationService.EncodeRequestBody(new List<string> { "tulkojama virkne", "viens divi trīs" });
        };

        /*
        ***************************************************
        * TEST TRANSLATE AND LANGUAGE DIRECTION FUNCTIONS *
        ***************************************************
        */

        // Test Custom provider. Echoes translations and available systems.
        var customProvider = new CustomProviderService(customProviderUrl, customProviderApiKey, logger);
        // Translate strings.
        List<string> translations = await customProvider.TranslateAsync(srcLangCode, trgLangCode, translatableStrings);
        logger.LogInformation("Custom Provider Translations:");
        foreach (var translation in translations)
        {
            logger.LogInformation(translation);
        }

        // Retrieve systems.
        var systems = await customProvider.GetSupportedEngineListAsync();
        logger.LogInformation("Custom Provider Supported Engine List:");
        foreach (var system in systems.LanguageDirections)
        {
            logger.LogInformation($"Source: {system.SrcLang}, Target: {system.TrgLang}, Domain: {system.Domain}, Vendor: {system.Vendor}");
        }

        // Test eTranslation provider. Echoes translations and available systems.
        var etranslationProvider = new EtranslationService(etranslationAppName, etranslationPassword, etranslationResponseEndpointUrl, etranslationResponseAwaitFn, logger);
        // Translate strings.
        List<string> translations2 = await etranslationProvider.TranslateAsync(srcLangCode, trgLangCode, translatableStrings);
        logger.LogInformation("Etranslation Provider Translations:");
        foreach (var translation in translations2)
        {
            logger.LogInformation(translation);
        }

        // Retrieve systems.
        var systems2 = await etranslationProvider.GetSupportedEngineListAsync();
        logger.LogInformation("Etranslation Provider Supported Engine List:");
        foreach (var system in systems2.LanguageDirections)
        {
            logger.LogInformation($"Source: {system.SrcLang}, Target: {system.TrgLang}, Domain: {system.Domain}, Vendor: {system.Vendor}");
        }
    }
}