﻿using System.Text.Json.Serialization;

namespace WEB_T_MT_API_Integrator.Model
{
    public class EtranslationDomain
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("languagePairs")]
        public List<string> LanguagePairs { get; set; }
    }
}
