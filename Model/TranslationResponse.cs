﻿using System.Text.Json.Serialization;

namespace WEB_T_MT_API_Integrator.Model
{
    public class TranslationResponse
    {
        [JsonPropertyName("domain")]
        public string Domain { get; set; }
        [JsonPropertyName("translations")]
        public List<TranslationValue> Translations { get; set; }
    }
}
