﻿using System.Text.Json.Serialization;

namespace WEB_T_MT_API_Integrator.Model
{
    public class LanguageDirection
    {
        [JsonPropertyName("srcLang")]
        public string SrcLang {  get; set; }
        [JsonPropertyName("trgLang")]
        public string TrgLang { get; set; }
        [JsonPropertyName("domain")]
        public string Domain { get; set; }
        [JsonPropertyName("vendor")]
        public string? Vendor { get; set; }
    }
}
