﻿using System.Text.Json.Serialization;

namespace WEB_T_MT_API_Integrator.Model
{
    public class LanguageDirectionsResponse
    {
        [JsonPropertyName("languageDirections")]
        public List<LanguageDirection> LanguageDirections { get; set; }
    }
}
