﻿using System.Text.Json.Serialization;

namespace WEB_T_MT_API_Integrator.Model
{
    public class TranslationValue
    {
        [JsonPropertyName("translation")]
        public string Translation {  get; set; }
    }
}
